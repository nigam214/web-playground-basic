#!/bin/bash

echo "###########################"
echo "# Basic Environment Setup #"
echo "###########################"

echo "# Install Packages for Bash Setup"
sudo apt-get update
sudo apt-get install -y bc
sudo apt-get install -y figlet
sudo apt-get install -y screenfetch

echo "# Setup Bash"
cp /vagrant/deploy/vagrant/server/bash_aliases /home/ubuntu/.bash_aliases
cp /vagrant/deploy/vagrant/server/bash_profile /home/ubuntu/.bash_profile

echo "# Setup VIM Configurations"
cp /vagrant/deploy/vagrant/server/vimrc /home/ubuntu/.vimrc

echo "###############"
echo "# XAMPP Setup #"
echo "###############"

echo "# Download and Install XAMPP"
mkdir -p /vagrant/deploy/vagrant/debug/softwares
cd /vagrant/deploy/vagrant/debug/softwares
if [ ! -f xampp-linux-x64-7.0.25-0-installer.run ] ; then
  echo "# Download XAMPP"
  wget -q https://www.apachefriends.org/xampp-files/7.0.25/xampp-linux-x64-7.0.25-0-installer.run
fi
sudo chmod +x xampp-linux-x64-7.0.25-0-installer.run
if [ ! -f /opt/lampp/xampp ] ; then
  echo "# Install XAMPP"
  sudo ./xampp-linux-x64-7.0.25-0-installer.run --mode unattended --disable-components xampp_developer_files
fi

echo "# Add XAMPP into the bashrc path"
echo 'PATH="/opt/lampp/bin:$PATH"' >> /home/ubuntu/.bashrc

echo "# Stop FTP Server on XAMPP"
sudo /opt/lampp/xampp stopftp
sudo /opt/lampp/xampp restart

echo "# Configure Apache Server"
sudo sed -i 's/#Include etc\/extra\/httpd-vhosts.conf/Include etc\/extra\/httpd-vhosts.conf/g' /opt/lampp/etc/httpd.conf
sudo sed -i 's/Require local/Require all granted/g' /opt/lampp/etc/extra/httpd-xampp.conf
sudo cp /vagrant/deploy/apache/vhost.conf /opt/lampp/etc/extra/httpd-vhosts.conf
if [ ! -d /opt/lampp/htdocs/playground ] ; then
  sudo mkdir -p /opt/lampp/htdocs
  sudo ln -s /vagrant /opt/lampp/htdocs/playground
fi

echo "# Run apache under user ubuntu instead of user daemon to resolve file access issue with php"
sudo sed -i 's/User daemon/User ubuntu/g' /opt/lampp/etc/httpd.conf
sudo sed -i 's/Group daemon/Group ubuntu/g' /opt/lampp/etc/httpd.conf

echo "# Add default user group ubuntu to server user daemon"
sudo usermod -a -G ubuntu daemon
groups ubuntu
groups daemon

# XAMPP on restart doesn't work because folder mounting in vagrant happen after restart
# echo "# Setup XAMPP to start on machine restart"
# sudo cp /vagrant/deploy/vagrant/server/init.d.lampp /etc/init.d/lampp
# sudo chmod +x /etc/init.d/lampp
# sudo update-rc.d lampp defaults

echo "########################"
echo "# PHPUnit Xdebug Setup #"
echo "########################"

echo "# Xdebug Setup"
sudo apt-get install -y php-xdebug
if [ `grep "xdebug.so" /opt/lampp/etc/php.ini | wc -l` == "0" ] ; then
  sudo sed -i 's/php_zip.dll/php_zip.dll\nzend_extension="\/usr\/lib\/php\/20151012\/xdebug.so"/g' /opt/lampp/etc/php.ini
fi

echo "###############"
echo "# MySQL Setup #"
echo "###############"

echo "# set mysql root password"
/opt/lampp/bin/mysql -u root -e "SET PASSWORD FOR 'root'@'localhost' = PASSWORD('root');"

echo "# Set root password in phpmyadmin"
sudo sed -i "s/\['password'] = ''/['password'] = 'root'/g" /opt/lampp/phpmyadmin/config.inc.php

echo "#################"
echo "# XAMPP Restart #"
echo "#################"

echo "# XAMPP Server Restart"
sudo /opt/lampp/xampp restart

echo "#########################"
echo "# Message for Next Step #"
echo "#########################"

echo "# echo message for next stage"
echo "vagrant ssh"
echo "/vagrant/deploy/vagrant/script/VagrantSetup.sh"

