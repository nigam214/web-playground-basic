#!/bin/bash

echo "# set mysql root password"
/opt/lampp/bin/mysql -u root -e "SET PASSWORD FOR 'root'@'localhost' = PASSWORD('root');"

echo "# Install Composer"
if [ ! -f /usr/local/bin/composer ] ; then
  php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
  php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
  php composer-setup.php
  php -r "unlink('composer-setup.php');"
  sudo mv composer.phar /usr/local/bin/composer
fi

echo "# Install Laravel"
if [ ! -f $HOME/.config/composer/vendor/bin/laravel ] ; then
  composer global require "laravel/installer"
  echo 'PATH="$HOME/.config/composer/vendor/bin:$PATH"' >> /home/ubuntu/.bashrc
fi

cd /vagrant
echo "# Restart Servers"
./deploy/script/server.restart.sh

